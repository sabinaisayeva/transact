﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeKata
{
    public static class Gaderypoluki
    {
        /// <summary>
        /// Encodes the Text with the specified key
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="encodedText">The encoded text.</param>
        /// <returns></returns>
        public static Tuple<string, int[]> Encode(string key, string encodedText)
        {
                CheckKey(key);
                List<string> keys = DivideKey(key.ToLower());
                keys.Sort((x, y) => String.Compare(x, y, StringComparison.CurrentCultureIgnoreCase));
                return MakeReplacements(keys, encodedText.ToLower());

        }
        /// <summary>
        /// Check the key is even and chars are unique
        /// </summary>
        /// <param name="key">The key.</param>
        /// <exception cref="System.ArgumentException">
        /// The number of letters in the encryption key must be even.
        /// or
        /// The encryption key is invalid. Each letter in the entire key must be unique.
        /// </exception>
        private static void CheckKey(string key)
        {
            if (key.Length % 2 != 0)
                throw new ArgumentException("The number of letters in the encryption key must be even.");
            if (!CheckUniqueLetter(key))
                throw new ArgumentException("The encryption key is invalid. Each letter in the entire key must be unique.");

        }

        /// <summary>
        /// Checks if all letters in key are unique
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        private static bool CheckUniqueLetter(string key)
        {
            bool[] array = new bool[256];
            foreach (char value in key)
                if (array[(int)value])
                    return false;
                else
                    array[(int)value] = true;
            return true;
        }

        /// <summary>
        /// Divides the key to list by two chars
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        private static List<string> DivideKey(string key)
        {
            List<string> dividedKey = new List<string>();
            for (int i = 0; i < key.Length; i+=2)
            {
                dividedKey.Add(key.Substring(i,2));
            }
            return dividedKey;
        }
        /// <summary>
        /// Makes the replacements based on key
        /// </summary>
        /// <param name="keys">The keys.</param>
        /// <param name="encodedText">The encoded text.</param>
        /// <returns></returns>
        private static Tuple<string, int[]> MakeReplacements(List<string> keys, string encodedText)
        {
            int[] indexes = new int[keys.Count+1];
            int i = 0;
            foreach (string phrase in keys)
            {
                    //'#' and '*' serves for not doing replacement like (bale) a->b (bble) then b->a (aale) instead of (able)
                    indexes[i] = encodedText.Count(x => x == phrase[0] || x==phrase[1]);
                    encodedText = encodedText.Replace(phrase[0], '#');
                    encodedText = encodedText.Replace(phrase[1], '*');
                    encodedText = encodedText.Replace('#', phrase[1]);
                    encodedText = encodedText.Replace('*', phrase[0]);
                    i++;
            
            }
            return Tuple.Create(encodedText, indexes);
        }

    }
}
